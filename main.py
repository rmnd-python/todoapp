from datetime import date, timedelta
from typing import Any, Literal
import json

PRIORITIES = ('Low', 'Medium', 'High')
TASK_STATUSES = ('Not started', 'In progress', 'Done')
SORT_BY = ['status', 'priority', 'duedate', 'time']
INPUT_TEXT_DATA = {
    'start': "Welcome to our ToDo app! We've just created New ToDo list for you. "
             "\n- If you want to keep using this Empty List, ENTER 1. \n- If you have a Saved List and "
             "want to backup, ENTER 2. \n- To terminate the program, ENTER 0."
             "\nInput: ",
    'manage': f"\nChoose and action:"
            f"\n- ENTER 1 to ADD new task;\n- ENTER 2 to DISPLAY all your tasks;"
            f"\n- ENTER 3 to UPDATE a task;\n- ENTER 4 to DELETE the task;"
            f"\n- To terminate the program, ENTER 0."
            f"\nInput: ",
    'title': "Enter UNIQUE task title: ",
    'priorities': "Enter task PRIORITY: 1 for LOW, 2 for MEDIUM, 3 for HIGH: ",
    'desc': "Enter task description (any notes); press Enter to leave it empty: ",
    'deadline': "Enter AMOUNT OF DAYS you have to complete the task: 0 for today, 7 for 1 week, e.t.c: ",
    'time': "Enter amount of TIME you need to complete the task, in hours: "
            "at least 1 and at most 24. If the time exceeds 24, consider splitting it to subtasks.\nEnter INTEGER: ",
    'new_status': "Enter 1 to set the Task STATUS. Type 0 or press Enter to leave as default (NOT STARTED): ",
    'set_status': "Enter current task progress: 1 for NOT STARTED, 2 for IN PROGRESS, 3 for COMPLETED: ",
    'progress': "Enter the progress of the task in percentages (INTEGER from 0 to 100). "
                "\nSetting of 100 will automatically assign task as completed. Input number: ",
    'start_update': "Input the Task title to UPDATE. Input 0 to CANCEL.\nInput: ",
    'do_update': "Now you are supposed to input New Data, except the Title (because it acts as Unique ID).\n",
    'start_deletion': "Input the Task title to DELETE. Input 0 to CANCEL.\nInput: ",
    'confirm_deletion': "Input 1 to CONFIRM the action. Input 0 to CANCEL.\nInput: ",
    'sort': "How would you like to sort tasks?\n"
            "- Enter 1 or Press Enter to sort by STATUS;\n"
            "- Enter 2 to sort by PRIORITY;\n"
            "- Enter 3 to sort by DUE DATE;\n"
            "- Enter 4 to sort by TIME ESTIMATION;\nInput: "
}

def get_weekday(days: int) -> str:
    return (
            ('Next ' if days == 7 else '') + (date.today() + timedelta(days=days)).strftime("%A")
            ) if days > 0 else "Today"


def print_columns(items: list) -> None:
    items = list(map(
        lambda item: f"{item: ^16}",
        items
    ))
    # items = list([f"||{it: ^16}" for it in items])
    items[0] = f"{items[0]: ^24}"
    items[-1] = f"{items[-1]: ^48}"
    print(*items, sep='||', end=f"\n{'-':-^160}\n")

def validate_value(value: Any, validation_type: str) -> bool:
    valid = True
    try:
        match validation_type:
            case 'priorities':
                assert 1 <= value <= 3

            case 'deadline':
                assert 0 <= value <= 7
            case 'time':
                assert 0 <= value <= 24
            case 'confirm':
                assert value in (0, 1)
            case 'start':
                assert value in (0, 1, 2)
            case 'manage':
                assert value in (0, 1, 2, 3, 4)
            case 'sort':
                assert value in ('', '1', '2', '3', '4')
            case 'new_status':
                assert value in (0, 1, '')
            case 'status':
                assert value in (1, 2, 3)
            case 'progress':
                assert 0 <= value <= 100
    except AssertionError:
        valid = False
    return valid

def get_value_from_keyboard(prompt: str, to_type: type,
                            validation_type: Literal[
                                'priorities', 'progress', 'deadline', 'time', 'confirm',
                                'new_status', 'status', 'progress', 'start', 'manage', 'sort'
                            ] = None) -> Any:
    """ Recursive function to get the input."""
    print(f"{'=':=^100}")
    try:
        value = to_type(input(prompt))
        if validation_type:
            if not validate_value(value, validation_type=validation_type):
                raise ValueError
        return value
    except ValueError:
        print(f"Invalid value! Try again and make sure you follow the instructions!")
        return get_value_from_keyboard(prompt, to_type, validation_type=validation_type)


class Task:
    """ Class thar represents one single task."""
    def __init__(self,
                 title: str,
                 priority: int,
                 description: str,
                 days_to_complete: int,
                 progress: int,
                 status: int = 1,
                 time_estimate: int = 0,

    ):
        self.title = title
        self.priority = priority
        self.description = description
        self.days_to_complete = days_to_complete
        self.status = status
        self.time_estimate = time_estimate
        self.progress = progress
        if progress == 100: self.status = 3

    def print_task(self) -> None:
        print_columns([
            self.title, PRIORITIES[self.priority - 1], get_weekday(self.days_to_complete),
            TASK_STATUSES[self.status - 1], self.time_estimate,
            f"{self.progress}%",
            self.description
        ])

def create_new_task(is_new=True, title=None):
    if is_new:
        title = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["title"], to_type=str)
    else: assert title
    description = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["desc"], to_type=str)
    priority = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["priorities"], validation_type='priorities', to_type=int)
    deadline = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["deadline"], validation_type='deadline', to_type=int)
    time_estimation = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["time"], validation_type='time', to_type=int)
    set_status = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["new_status"], validation_type='new_status', to_type=int)
    progress = 0
    if set_status:
        status = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["set_status"], validation_type='status', to_type=int)
        if status == 2:  # In progress
            progress = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["progress"], validation_type='progress', to_type=int)
    else: status = 1

    task = Task(
        title=title,
        description=description,
        priority=priority,
        days_to_complete=deadline,
        status=status,
        time_estimate=time_estimation,
        progress=progress
    )
    return task

class ToDoList:
    """ Class that represents the ToDo list """
    def __init__(self):
        self.tasks = set()

    def add_task(self, task) -> None:
        if task.title == '0':
            print(f"Task with name {task.title} CANNOT be added. Please choose another Name!\n")
        elif any(task.title == t.title for t in self.tasks):
            print(f"Task with name {task.title} already exists and CANNOT be added. Choose another Name!\n")
        else:
            self.tasks.add(task)

    def update_task(self) -> None:
        name = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["start_update"], to_type=str)
        if not any(task.title == name for task in self.tasks):
            if name != '0':
                print(f"Task with title '{name}' not found.", end=' ')
            return print("Update cancelled.")
        print(INPUT_TEXT_DATA["do_update"])
        new_task = create_new_task(is_new=False, title=name)
        for task in self.tasks:
            if task.title == name:
                self.tasks.remove(task)
                self.tasks.add(new_task)
                break

        print(f"Task '{name}' was updated successfully!\n")

    def delete_task(self) -> None:
        name = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["start_deletion"], to_type=str)
        if any(task.title == name for task in self.tasks):
            if get_value_from_keyboard(prompt=INPUT_TEXT_DATA["confirm_deletion"], validation_type='confirm', to_type=bool):
                self.tasks = set([task for task in self.tasks if task.title != name])
                return print(f"Task '{name}' was deleted successfully!\n")
        else:
            print(f"Task with title '{name}' wasn't found.", end=' ')
        return print("Deletion cancelled.\n")


    def sort_tasks(self, criterion: str) -> list:
        reverse: bool = True
        match criterion:
            case 'completion':
                keys = ("status", "progress")
            case 'duedate':
                keys = ("days_to_complete",)
                reverse = False
            case 'time':
                keys = ("time_estimate",)

            case 'priority' | _:
                keys = ("priority",)

        tasks = sorted(self.tasks, key=lambda task: tuple([getattr(task, k) for k in keys]))
        if reverse:
            tasks = reversed(tasks)

        return tasks
    def show_tasks(self) -> None:
        if not self.tasks:
            return print("You don't have saved Tasks yet :(\nBut it's great time to begin!")
        sort_by = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["sort"], validation_type='sort', to_type=str)
        sort_by = int(sort_by or 1)
        tasks = self.sort_tasks(criterion=SORT_BY[sort_by - 1])
        print(f"{'~':~^160}\n{'YOUR TASKS:':^150}\n{'~':~^160}")
        print_columns(["Title", "Priority:", "Due:", "Status:", "Time Estimate:", "Progress:", "Description:"])
        for task in tasks:
            task.print_task()

    def save_to_file(self):
        file_name = "ToDo.json"
        with open(file_name, 'w') as file:
            task_list = [task.__dict__ for task in self.tasks]
            json.dump(task_list, file)
        print(f"\n{'+':+^100}\n+++{f'Your data is up to date and stored under: {file_name}.':^94}+++\n{'+':+^100}\n")

    def load_from_file(self, filename):
        try:
            with open(filename, 'r') as file:
                task_list = json.load(file)
                self.tasks = {Task(**task) for task in task_list}
            print(f"Saved ToDo was loaded successfully!")

        except FileNotFoundError:
            print(f"File {filename} not found. Starting with an empty ToDo list.")

    def manage_todo(self):
        action = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["manage"], to_type=int, validation_type='manage')
        match action:
            case 0:
                return
            case 1:
                self.add_task(create_new_task())
            case 2:
                self.show_tasks()
            case 3:
                self.update_task()
            case 4:
                self.delete_task()
        self.save_to_file()
        self.manage_todo()


def new_ToDo(from_file: bool = False) -> ToDoList:
    todo = ToDoList()
    if from_file:
        try:
            todo.load_from_file("ToDo.json")
        except FileNotFoundError:
            print("Tried to find saved ToDo, but file wasn't found. New ToDo List is used.")
    return todo


def main():
    first_action = get_value_from_keyboard(prompt=INPUT_TEXT_DATA["start"], to_type=int, validation_type='start')
    if not first_action:
        return
    todo_list = new_ToDo(bool(first_action - 1))
    todo_list.manage_todo()
    print("Program Stopped! All data is saved. Wish you come back!")

if __name__ == "__main__":
    main()
