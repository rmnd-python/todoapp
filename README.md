# ToDo Console App

## Overview
This is a simple console-based ToDo app written in Python. The project aims to demonstrate the use of Object-Oriented Programming (OOP) concepts in Python. It provides a basic yet functional ToDo list management system with the flexibility to add, update, display, and delete tasks.

## Features
- **Console App**: The ToDo app is a command-line application designed to run in the console or terminal.
- **Object-Oriented Programming**: The project showcases the use of OOP principles in Python, with classes like `Task` and `ToDoList`.
- **No External Libraries Required**: The app is built using standard Python libraries, making it easy to run without the need for additional installations.

## Recursion Usage
The project utilizes recursion in certain scenarios, showcasing an alternative approach to traditional while loops. This demonstrates the flexibility and elegance of recursive functions, even in situations where while loops might seem more conventional.

## System Requirements
- **Python Version**: Python 3.10 or higher is required to run the ToDo app.

## Usage
1. Clone the repository:
   ```bash
   git clone https://gitlab.com/rmnd-python/todoapp.git
   cd ToDo-console-app
   ```
